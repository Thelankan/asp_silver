package com.prevail.automation.pageObjects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.prevail.automation.Actions.SeleniumActions;
import com.prevail.automation.Utility.FetchDataUtility;

public class MiniCartObjects extends SeleniumActions {

	private static Logger Log = Logger.getLogger(HomePage.class);
	private WebDriver driver;
	private WebDriverWait wait;
	public HeaderPageObject headerPage;

	@FindBy(xpath = "//div//a[@class='minicart-link']")
	private WebElement miniCartIcon;

	@FindBy(xpath = "//*[@class='add-to-cart btn btn-primary']")
	public WebElement addToCart;

	@FindBy(xpath = "//span[@class='minicart-quantity']")
	private WebElement miniCartProductNum;

	@FindBy(xpath = "//div[@class='line-item-name']")
	private WebElement productInViewCartPage;

	@FindBy(xpath = "//html[@id='ext-gen45']//a[@class='continue-shopping-link']")
	private WebElement mncrt_continueShopping;

	@FindBy(xpath = "//button[contains(@class,'remove-btn-lg')]")
	private WebElement mncrt_removeBtn;

	@FindBy(xpath = "//*[@class='container cart']//h4[contains(.,'Your shopping cart')]")
	private WebElement viewcrt_yourShoppingCart;

	@FindBy(xpath = "//*[@class='container cart']//a[contains(.,'View Cart')]")
	private WebElement mncrt_viewCart;

	@FindBy(xpath = "//*[starts-with(@class,'row align')]//span[contains(.,'Each')]")
	private WebElement mncrt_inStock_each;

	@FindBy(xpath = "//*[@class='quantity-form']//label[contains(.,'Quantity')]")
	private WebElement mncrt_inStock_quantity;

	@FindBy(xpath = "//*[starts-with(@class,line-item)]//span[contains(.,'Total')]")
	private WebElement mncrt_inStock_total;

	@FindBy(xpath = "//*[starts-with(@class,'col')]//p[contains(.,'Estimated Total')]")
	private WebElement mncrt_inStock_estimatedTotal;

	@FindBy(xpath = "(//*[starts-with(@class,'col')]//a[contains(.,'Checkout')])[1]")
	private WebElement mncrt_inStock_checkOut;

	@FindBy(xpath = "//div[@class='remove-line-item']/button[contains(@class,'remove')]")
	private WebElement mncrt_removeButton;

	@FindBy(xpath = "//div[@id='removeProductModal']//button[contains(.,'Yes')]")
	private WebElement mncrt_deleteYesBtn_popUp;

	@FindBy(xpath = "//div[@class='page']//h1[contains(.,'Your Shopping Cart is Empty')]")
	private WebElement mncrt_cartEmptyMessage;

	@FindBy(xpath = "//div[@class='add-to-cart-messages']")
	private WebElement mncrt_addToCartMsg;

	@FindBy(xpath = "html//span[@itemprop='price']")
	private WebElement mncrt_product_cost;

	@FindBy(xpath = "html//span[@itemprop='price']")
	private WebElement mncrt_popup_price;

	@FindBy(xpath = "html//select[starts-with(@class,'quantity-select')]")
	private WebElement mncrt_dropdwn_quantity;

	@FindBy(xpath = "html//p[contains(@class,'text-right sub-total')]")
	public WebElement mncrt_estimated_total;

	@FindBy(xpath = "html//button[contains(@class,'btn btn-outline-primary')]")
	private WebElement sp_moreItems;

	@FindBy(xpath = "//*[@id=\"product-search-results\"]/div[1]/div[1]/span[1]")
	private WebElement sp_totalNoOfSearchResults;

	// Constructor for minicart PageObjects
	public MiniCartObjects(WebDriver driver) {
		Log.info("Home Page constructor is Invoked");
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Tests to ad product in cart and verify the name of the product
	 * 
	 * @throws Exception
	 */
	public void clikOnMiniCart() throws Exception {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(miniCartIcon));
		clickElement(driver, miniCartIcon);
		String expectedProductName = FetchDataUtility.getCellData(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart", 1, 1);
		String locator = "//div[@id='product-search-results']//a[contains(.,'Name')]";
		String replaceString = locator.replaceAll("Name", expectedProductName);
		driver.findElement(By.xpath(replaceString)).click();
		wait.until(ExpectedConditions.visibilityOf(addToCart));
		clickElement(driver, addToCart);
		wait = new WebDriverWait(driver, 60);
		mouseoverElement(driver, addToCart);
	}

	/**
	 * Adds a product to cart and verifies the same
	 * 
	 * @throws Exception
	 */
	public void addToCart() throws Exception {
		sleepinSeconds(2);
		headerPage = new HeaderPageObject(driver);
		headerPage.clickOnTopSeller();
		String expectedProductName = FetchDataUtility.getCellData(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart", 2, 3);
		String locator = "//div[@id='product-search-results']//a[contains(.,'Name')]";
		String replaceString = locator.replaceAll("Name", expectedProductName);
		driver.findElement(By.xpath(replaceString)).click();
		sleepinSeconds(2);
		clickElement(driver, addToCart);
	}

	public void addAllToCart() throws Exception {
		sleepinSeconds(2);
		headerPage = new HeaderPageObject(driver);
		List<String> expectedProductName = FetchDataUtility.getRowWithCellValue(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart",
				"MultipleProducts");
		for (int i = 0; i < expectedProductName.size(); i++) {
			if (!(i == 0)) {
				wait.until(ExpectedConditions.invisibilityOf(mncrt_addToCartMsg));
			} else {
				sleepinSeconds(7);
			}
			headerPage.clickOnTopSeller();
			sleepinSeconds(2);
			if (!(expectedProductName.get(i) == "")) {
				String locator = "//div[@id='product-search-results']//a[contains(.,'Name')]";
				String replaceString = locator.replaceAll("Name", expectedProductName.get(i));
				sleepinSeconds(2);
				WebElement productLink = driver.findElement(By.xpath(replaceString));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", productLink);
				sleepinSeconds(2);
				productLink.click();
				wait.until(ExpectedConditions.elementToBeClickable(miniCartProductNum));
				clickElement(driver, addToCart);
				sleepinSeconds(2);
			}
		}
	}

	/**
	 * Adds the products to cart and captures the amount
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public List<String> addAlltocartNGetLabels() throws Exception {
		sleepinSeconds(2);
		headerPage = new HeaderPageObject(driver);
		List<String> productCost = new ArrayList<String>();
		List<String> expectedProductName = FetchDataUtility.getRowWithCellValue(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart",
				"MultipleProducts");
		for (int i = 0; i < expectedProductName.size(); i++) {
			if (!(i == 0)) {
				wait.until(ExpectedConditions.invisibilityOf(mncrt_addToCartMsg));
			} else {
				sleepinSeconds(2);
			}
			headerPage.clickOnTopSeller();
			sleepinSeconds(1);
			String locator = "//div[@id='product-search-results']//a[contains(.,'Name')]";
			String replaceString = locator.replaceAll("Name", expectedProductName.get(i));
			sleepinSeconds(1);
			WebElement productLink = driver.findElement(By.xpath(replaceString));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", productLink);
			sleepinSeconds(1);
			productLink.click();
			productCost.add(getText(driver, mncrt_product_cost));
			wait.until(ExpectedConditions.elementToBeClickable(miniCartProductNum));
			clickElement(driver, addToCart);
			sleepinSeconds(1);
		}
		return productCost;
	}

	/**
	 * Verifies the number of product in cart displayed in 'Mini Cart' icon
	 * 
	 * @return
	 */
	public String miniCartProductNum() {
		sleepinSeconds(2);
		wait.until(ExpectedConditions.visibilityOf(miniCartProductNum));
		return getText(driver, miniCartProductNum);
	}

	/**
	 * Mouse over on 'Mini Cart' icon and click on 'View Cart' link
	 * 
	 * @return
	 */
	public String clickOnviewCart() {
		sleepinSeconds(2);
		mouseoverElement(driver, miniCartIcon);
		wait.until(ExpectedConditions.visibilityOf(mncrt_viewCart));
		clickElement(driver, mncrt_viewCart);
		wait.until(ExpectedConditions.visibilityOf(productInViewCartPage));
		String prodName = getText(driver, productInViewCartPage);
		return prodName;
	}

	/**
	 * Mouse over on 'Mini Cart' icon and click on 'View Cart' link
	 * 
	 * @param locator
	 * 
	 * @return
	 */
	public List<String> clickOnviewCartGetListItems(String locator) {
		sleepinSeconds(2);
		mouseoverElement(driver, miniCartIcon);
		wait.until(ExpectedConditions.visibilityOf(mncrt_viewCart));
		clickElement(driver, mncrt_viewCart);
		wait.until(ExpectedConditions.visibilityOf(productInViewCartPage));
		List<WebElement> prodName = getList(driver, locator);
		List<String> elementName = new ArrayList<String>();
		Iterator<WebElement> i = prodName.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			elementName.add(row.getText());
		}
		return elementName;
	}

	/**
	 * Clicks on Coninue Shopping
	 */
	public void clickContinueshopping() {
		clickElement(driver, mncrt_continueShopping);
	}

	/**
	 * removes all product which can be viewed on mouse over of mini cart
	 */
	public void emptyCart() {
		sleepinSeconds(2);
		mouseoverElement(driver, miniCartIcon);
		sleepinSeconds(1);
		clickElement(driver, mncrt_removeButton);
		sleepinSeconds(2);
		wait.until(ExpectedConditions.visibilityOf(mncrt_deleteYesBtn_popUp));
		clickElement(driver, mncrt_deleteYesBtn_popUp);
		sleepinSeconds(2);
	}

	public void emptyAllCart() {
		sleepinSeconds(2);
		mouseoverElement(driver, miniCartIcon);
		do {
			wait.until(ExpectedConditions.visibilityOf(mncrt_removeButton));
			clickElement(driver, mncrt_removeButton);
			wait.until(ExpectedConditions.visibilityOf(mncrt_deleteYesBtn_popUp));
			clickElement(driver, mncrt_deleteYesBtn_popUp);
		} while (mncrt_removeButton.isDisplayed());
	}

	/**
	 * Gets the list of labels displayed in 'Mini Cart' icon
	 * 
	 * @return
	 */
	public List<String> getMiniCartLabels() {
		List<String> MiniCartLabel = new ArrayList<String>();
		MiniCartLabel.add(getText(driver, viewcrt_yourShoppingCart));
		MiniCartLabel.add(getText(driver, mncrt_viewCart));
		MiniCartLabel.add(getText(driver, mncrt_inStock_each));
		MiniCartLabel.add(getText(driver, mncrt_inStock_quantity));
		MiniCartLabel.add(getText(driver, mncrt_inStock_total));
		MiniCartLabel.add(getText(driver, mncrt_inStock_estimatedTotal));
		MiniCartLabel.add(getText(driver, mncrt_inStock_checkOut));
		return MiniCartLabel;
	}

	/**
	 * Returns pop up message from minicart
	 * 
	 * @return
	 */
	public String getMiniCartPopUpMsg() {
		wait.until(ExpectedConditions.visibilityOf(mncrt_addToCartMsg));
		String expMsg = getText(driver, mncrt_addToCartMsg);
		return expMsg;
	}

	/**
	 * Used to increase quantity
	 * 
	 * @param expectedProductName
	 * @return
	 */
	public float addAndIncreaseQauntty(List<String> expectedProductName) {
		float y = 0, z = 0;
		for (int i = 0; i < expectedProductName.size(); i++) {
			if (!(expectedProductName.get(i) == "")) {
				sleepinSeconds(2);
				headerPage = new HeaderPageObject(driver);
				headerPage.clickOnTopSeller();
				sleepinSeconds(1);
				String locator = "//div[@id='product-search-results']//a[contains(.,'Name')]";
				String replaceString = locator.replaceAll("Name", expectedProductName.get(i));
				sleepinSeconds(2);
				WebElement productLink = driver.findElement(By.xpath(replaceString));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", productLink);
				sleepinSeconds(2);
				productLink.click();
				wait.until(ExpectedConditions.elementToBeClickable(miniCartProductNum));
				if (i == 0) {
					selectElement(driver, mncrt_dropdwn_quantity, "2");
					sleepinSeconds(2);
					clickElement(driver, addToCart);
					sleepinSeconds(2);
					String price1 = (getText(driver, mncrt_product_cost));
					y = convertPriceToFloat(price1);
				} else {
					clickElement(driver, addToCart);
					mouseoverElement(driver, miniCartIcon);
					sleepinSeconds(2);
					String locatoricon = "(html//span[contains(.,'Name')]/following::div//select)[1]";
					String replacedString = locatoricon.replace("Name", expectedProductName.get(i));
					mouseoverElement(driver, driver.findElement(By.xpath(replacedString)));
					selectElement(driver, driver.findElement(By.xpath(replacedString)), "2");
					String priceloc = "html//span[contains(.,'Name')]/following::div[starts-with(@class,'pricing')]";
					String replacePiceloc = priceloc.replace("Name", expectedProductName.get(i));
					sleepinSeconds(2);
					String price2 = getText(driver, driver.findElement(By.xpath(replacePiceloc)));
					z = convertPriceToFloat(price2);
				}
				sleepinSeconds(2);
			}
		}
		float total;
		return total = (y * 2) + z;
	}

	/**
	 * Covert price to float by removing '$' and ',' sign
	 * 
	 * @param price
	 * @return
	 */
	public float convertPriceToFloat(String price) {
		price = price.replace("$", "");
		price = price.replace(",", "");
		Float num = Float.parseFloat(price);
		return num;
	}

	public void mouseOverMiniCart() {
		mouseoverElement(driver, miniCartIcon);
	}

	public int TotalitemInPlp(int defaultList) {
		int TotalNoOfItems = SearchResultCount();
		int x = TotalNoOfItems / defaultList;
		for (int i = 0; i < x; i++) {
			sleepinSeconds(1);
			clickElement(driver, sp_moreItems);
			sleepinSeconds(2);
		}
		int totalNoOfItemsOnPage = driver.findElements(By.xpath("//div[@class ='image-container']")).size();
		return totalNoOfItemsOnPage;
	}

	public int SearchResultCount() {
		String[] str = getSearchPageText("TotalNoOfSearchResult").split(" ");
		String TotalNoOfItemsInSearchResult = str[0];
		System.out.println("TotalNoOfItemsInSearchResult-------------------" + TotalNoOfItemsInSearchResult);
		int TotalNoOfItems = Integer.parseInt(TotalNoOfItemsInSearchResult);
		return TotalNoOfItems;
	}

	public void addAllToCartFromDiffPage() throws Exception {
		sleepinSeconds(2);
		headerPage = new HeaderPageObject(driver);
		List<String> expectedProductName = FetchDataUtility.getRowWithCellValue(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart",
				"MultiPageProducts");
		for (int i = 0; i < expectedProductName.size(); i++) {
			if (!(i == 0)) {
				wait.until(ExpectedConditions.invisibilityOf(mncrt_addToCartMsg));
			} else {
				sleepinSeconds(7);
			}
			headerPage.clickOnTopSeller();
			sleepinSeconds(2);
			String locator = "//div[@id='product-search-results']//a[contains(.,'Name')]";
			String replaceString = locator.replaceAll("Name", expectedProductName.get(i));
			sleepinSeconds(2);
			WebElement productLink = driver.findElement(By.xpath(replaceString));
			try {
				if (productLink.isDisplayed())
					;
			} catch (NoSuchElementException e) {
				TotalitemInPlp(12);
			}
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", productLink);
			sleepinSeconds(2);
			productLink.click();
			wait.until(ExpectedConditions.elementToBeClickable(miniCartProductNum));
			clickElement(driver, addToCart);
			sleepinSeconds(2);
		}
	}

	private String getSearchPageText(String string) {
		String text = sp_totalNoOfSearchResults.getText();
		return text;
	}

}
