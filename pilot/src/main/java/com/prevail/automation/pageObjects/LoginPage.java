package com.prevail.automation.pageObjects;

import static org.testng.AssertJUnit.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.prevail.automation.Actions.SeleniumActions;

public class LoginPage extends SeleniumActions{

	private static Logger Log = Logger.getLogger(LoginPage.class);
	private WebDriver driver;
	private WebDriverWait wait;

	// -----------------------------------------------------Login section webElement/PageObjects-----------------------------

	@FindBy(xpath = "//*[@class='nav-link active']")
	private WebElement switchToLogin;

	@FindBy(xpath = "//*[@id=\"login-form-email\"]")
	private WebElement lp_loginEmail;

	@FindBy(xpath = "//*[@id=\"login-form-password\"]")
	private WebElement lp_password;

	@FindBy(xpath = "//*[@id=\"login-form-email\"]")
	private WebElement chk_remmMe ;

	@FindBy(xpath = "//*[@id=\"login\"]/form[1]/button")
	private WebElement lp_loginButton;

	@FindBy(xpath = "//*[@id=\"login\"]/form[2]/div[1]/a")
	private WebElement lp_loginUsingGoogle;

	@FindBy(xpath = "//*[@id=\"login\"]/form[2]/div[2]/a")
	private WebElement lp_loginUsingFacebook;

	@FindBy(xpath = "//*[@id=\"password-reset\"]")
	private WebElement lp_forgetPassword;

	@FindBy(xpath = "//*[@class= 'page-title']")
	private WebElement lp_getH1TitleOfPage;
	
	@FindBy(xpath ="//*[@id=\"login\"]/form[1]/div[1]")
	private WebElement errorMsgLogin1;
	
	@FindBy (xpath = "//*[@id=\"login\"]/form[1]/div[1]/div")
	private WebElement errorMsgLongin2;
	
	@FindBy (css = ".page-title")
	private WebElement dashboard;
	
	


	// ----------------------------------------------------Registration section webElement/PageObjects------------------------------

	@FindBy(xpath = "//a[@href='#register']")
	private WebElement lp_switchToRegistration;

	@FindBy(xpath = "//*[@id=\"registration-form-fname\"]")
	private WebElement lp_firstName;


	@FindBy(xpath = "//*[@id=\"registration-form-lname\"]")
	private WebElement lp_lastName;

	@FindBy(xpath = "//*[@id=\"registration-form-phone\"]")
	private WebElement lp_phoneNumber;


	@FindBy(xpath = "//*[@id=\"registration-form-email\"]")
	private WebElement lp_email;

	@FindBy(xpath = "//*[@id=\"registration-form-email-confirm\"]")
	private WebElement lp_confirmEmail;


	@FindBy(xpath = "//*[@id=\"registration-form-password\"]")
	private WebElement lp_regPass;

	@FindBy(xpath = "//*[@id=\"registration-form-password-confirm\"]")
	private WebElement lp_ConfirmRegPass;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[8]/label")
	private WebElement lp_addToMailListCheckbox;

	@FindBy(xpath = "//*[@id=\"register\"]/form/button")
	private WebElement lp_createAccountButton;

	@FindBy(xpath = "//*[@id=\"register\"]/form/button")
	private WebElement lp_privacyPolicy;



	// ----------------------------------------------------Check Order section webElement/PageObjects------------------------------
	@FindBy(css = "#trackorder-form-number")
	private WebElement lp_orderNum;

	@FindBy(css = "#trackorder-form-email")
	private WebElement lp_orderEmail;

	@FindBy(css = "#trackorder-form-zip")
	private WebElement lp_billingZip;

	@FindBy(css = "body > div.page > div.container.login-page > div.row.justify-content-center.equal-height > div:nth-child(2) > div > form > div > div:nth-child(6) > button")
	private WebElement lp_checkStatus;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/div[3]/div[2]/div[1]/div[1]/div[2]/dl[1]/dd")
	private WebElement lp_expectedName;

	@FindBy(xpath = "//*[@class='user-message']")
	private WebElement  lp_actualUserName;
	
	@FindBy(xpath = "//*[@class='affirm btn btn-primary']")
	private WebElement lp_popUp;
	
	@FindBy(css = ".alert > strong:nth-child(1)")
	private WebElement checkOrderErrorText;
	
	@FindBy(css = "div.card-body:nth-child(2) > div:nth-child(3) > div:nth-child(3)")
	private WebElement requestedFormatEmail;
	
	@FindBy(css = "div.card-body:nth-child(2) > div:nth-child(4) > div:nth-child(3)")
	private WebElement requestedFormatBillCode;
	
	@FindBy(css = "div.card-body:nth-child(2) > div:nth-child(2) > div:nth-child(3)")
	private WebElement  orderNumberError;
	
	@FindBy (css = "div.card-body:nth-child(2) > div:nth-child(3) > div:nth-child(3)")
	private WebElement orderEmailError;
	                  
	@FindBy (css = "div.card-body:nth-child(2) > div:nth-child(4) > div:nth-child(3)")
	private WebElement billCodeErrror;
	
	

	// ----------------------------------------------------Login Module validation messages webElement/PageObjects------------------------------
	@FindBy(xpath = "//*[@id=\"login\"]/form[1]/div[1]/div")
	private WebElement validationMsgInvalidEmail;

	@FindBy(xpath = "//*[@id=\"login\"]/form[1]/div[1]")
	private WebElement incorrectEmailPassword;


	// ----------------------------------------------------Login labels webElement/PageObjects------------------------------
	//login labels
	@FindBy(xpath = "//*[@id=\"login\"]/form[1]/div[1]/label")
	private WebElement emailLabel;

	@FindBy(xpath = "//*[@id=\"login\"]/form[1]/div[2]/label")
	private WebElement PasswordLabel;

	@FindBy(xpath = "//*[@id=\"login\"]/form[1]/div[3]/div[1]/label")
	private WebElement rembmeLabel;


	// ----------------------------------------------------Registration Module validation messages webElement/PageObjects------------------------------
	@FindBy(xpath = "//*[@id=\"register\"]/form/div[1]/div")
	private WebElement lp_fnameValidation;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[2]/div")
	private WebElement lp_lnameValidation;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[3]/div")
	private WebElement lp_phoneNoValidation;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[4]/div")
	private WebElement lp_emailValidation;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[5]/div")
	private WebElement lp_confirmEmailValidation;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[6]/div")
	private WebElement lp_passwordValidation;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[7]/div")
	private WebElement lp_confirmPasswordValidation;


	// ----------------------------------------------------Registration labels webElement/PageObjects------------------------------
	
	@FindBy(xpath = "//*[@id=\"register\"]/form/div[1]/label")
	private WebElement lp_fnameLabel;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[2]/label")
	private WebElement lp_lnameLabel;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[3]/label")
	private WebElement lp_phoneNoLabel;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[4]/label")
	private WebElement lp_emailLabel;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[5]/label")
	private WebElement lp_confirmEmailLabel;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[6]/label")
	private WebElement lp_passwordLabel;

	@FindBy(xpath = "//*[@id=\"register\"]/form/div[7]/label")
	private WebElement lp_confirmPasswordLabel;




	//Constructor
	public LoginPage(WebDriver driver){

		Log.info("LogIn/UserRegistration page constructor is Invoked");
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		PageFactory.initElements(driver, this);
		wait.until(ExpectedConditions.titleContains("Salesforce Commerce Cloud"));
	}

    //Find page title
	public String LoginPageTitle(){ 

		wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOf(lp_getH1TitleOfPage));
		String text = lp_getH1TitleOfPage.getText();
		return text;	
	}

	//Get registration label and error message
	public String getRegistration_LabelsAndErrorMsg(String validation){ 

		String text = null;
		if(validation.equalsIgnoreCase("RegFname")) {
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(lp_fnameValidation));
			text = lp_fnameValidation.getText();}
		else if (validation.equalsIgnoreCase("RegLname")) {
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(lp_lnameValidation));
			text = lp_lnameValidation.getText();}
		else if (validation.equalsIgnoreCase("RegPhone")) {
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(lp_phoneNoValidation));
			text = lp_phoneNoValidation.getText();}
		else if (validation.equalsIgnoreCase("RegEmail")) {
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(lp_emailValidation));
			text = lp_emailValidation.getText();}
		else if (validation.equalsIgnoreCase("RegConfirmEmail")) {
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(lp_confirmEmailValidation));
			text = lp_confirmEmailValidation.getText();}
		else if (validation.equalsIgnoreCase("RegPassword")) {
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(lp_passwordValidation));
			text = lp_passwordValidation.getText();}
		else if (validation.equalsIgnoreCase("RegConfirmPassword")) {
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(lp_confirmPasswordValidation));
			text = lp_confirmPasswordValidation.getText();} 
		return text;	

	}


	//Get Login labels and error Messages
	public String getLoginPage_LabelsAndErrorMsg(String label){

		String text = null;
		if(label.equalsIgnoreCase("Email"))
			text = emailLabel.getText();
		else if(label.equalsIgnoreCase("Password"))
			text = PasswordLabel.getText();	
		else if(label.equalsIgnoreCase("Remember Me"))
			text = rembmeLabel.getText();
		else if(label.equalsIgnoreCase("Forget Password"))
			text = lp_forgetPassword.getText();	
		else if(label.equalsIgnoreCase("Login"))
			text = lp_loginButton.getText();	
		else if(label.equalsIgnoreCase("GoogleLogin"))
			text = lp_loginUsingGoogle.getText();
		else if(label.equalsIgnoreCase("FacebookLogin"))
			text = lp_loginUsingFacebook.getText();
		else if(label.equalsIgnoreCase("LogedinPersonName"))
			text = lp_actualUserName.getText();
		else if(label.equalsIgnoreCase("RegFname"))
			text = lp_fnameLabel.getText();	
		else if(label.equalsIgnoreCase("RegLname"))
			text = lp_lnameLabel.getText();
		else if(label.equalsIgnoreCase("RegPhoneNo"))
			text = lp_phoneNoLabel.getText();
		else if(label.equalsIgnoreCase("RegEmail"))
			text = lp_emailLabel.getText();		
		else if(label.equalsIgnoreCase("RegConfirmEmail"))
			text = lp_confirmEmailLabel.getText();		
		else if(label.equalsIgnoreCase("RegPassword"))
			text = lp_passwordLabel.getText();	
		else if(label.equalsIgnoreCase("RegConfirmPassword"))
		    text = lp_confirmPasswordLabel.getText();	
		else if(label.equalsIgnoreCase("AddToMailList"))
			text = lp_addToMailListCheckbox.getText();
		else if(label.equalsIgnoreCase("CreateAccount"))
			text = lp_createAccountButton.getText();
		else if(label.equalsIgnoreCase("validateMsgInvalidMailID")){
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(validationMsgInvalidEmail));
			text = validationMsgInvalidEmail.getText();
			}
		else if(label.equalsIgnoreCase("validateMsgIncorrectEmailPass")){
			
			wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.visibilityOf(incorrectEmailPassword));
			text = incorrectEmailPassword.getText();
			}

		return text;	
	}


    //Login method
	public void login(String User , String Pass) throws InterruptedException {   
		
		clickElement(driver, switchToLogin);
		setText(driver, lp_loginEmail, User);
		setText(driver, lp_password, Pass);
		//clickElement(driver, checkbox);
		clickElement(driver, lp_loginButton);
		sleepinSeconds(3); 	 	     
	}

	
	//Login with Google method
	public LoginPage loginWithGoogle(String options){
		clickElement(driver, lp_loginUsingGoogle);
	//functionality not there
	return new LoginPage(driver);
	}

	
	//Login with Facebook Method
	public LoginPage loginWithFacebook(String options){
		clickElement(driver, lp_loginUsingFacebook);
	//functionality not there
	return new LoginPage(driver);
	}

	//Forget Password Method
	public LoginPage forgetPassword(String options){
		clickElement(driver, lp_forgetPassword);
	//functionality not there
	return new LoginPage(driver);
	}

     //Registration with add me to mail list check box checked
	public void RegistrationWithAddtoMailChecked(String Fname , String Lname, String Phone, String EmailId, String ConfirmEmailId, String Password, String ConfirmPassword) {  

		clickElement(driver, lp_switchToRegistration);  
		setText(driver, lp_firstName, Fname);
		setText(driver, lp_lastName, Lname);
		setText(driver, lp_phoneNumber, Phone );
		setText(driver, lp_email , EmailId);
		setText(driver, lp_confirmEmail , ConfirmEmailId);
		setText(driver, lp_regPass, Password);
		setText(driver, lp_ConfirmRegPass, ConfirmPassword);
		if(!(lp_addToMailListCheckbox.isSelected())) {
			clickElement(driver,lp_addToMailListCheckbox );}
		clickElement(driver,lp_createAccountButton );	
	}

	//Registration method when add me to mail list check box is not checked
	public void RegistrationWithoutAddtoMailChecked(String Fname , String Lname, String Phone, String EmailId, String ConfirmEmailId, String Password, String ConfirmPassword) {  

		clickElement(driver, lp_switchToRegistration);  
		setText(driver, lp_firstName, Fname);
		setText(driver, lp_lastName, Lname);
		setText(driver, lp_phoneNumber, Phone );
		setText(driver, lp_email , EmailId);
		setText(driver, lp_confirmEmail , ConfirmEmailId);
		setText(driver, lp_regPass, Password);
		setText(driver, lp_ConfirmRegPass, ConfirmPassword);
		if((lp_addToMailListCheckbox.isSelected())) {
			clickElement(driver,lp_addToMailListCheckbox );}
		clickElement(driver,lp_createAccountButton );	
	}
	

    //Privacy Policy method
	public LoginPage PrivacyPolicy(String options){  

		clickElement(driver, lp_privacyPolicy);
		//functionality not there
		return new LoginPage(driver);
	}

    //Check Order  Method
	public void CheckOrder(String OrderNumber, String OrderEmail, String Zip) {
		setText(driver, lp_orderNum, String.valueOf(OrderNumber));
		setText(driver, lp_orderEmail, OrderEmail);
		setText(driver, lp_billingZip, String.valueOf(Zip));
		clickElement(driver, lp_checkStatus);
		}
		
	// Check Order Method for all Invalid Data
	public String getErrorMsg(String Description) {
		if (Description.contains("Invalid Parameters")) {
			return checkOrderErrorText.getText();
		} else if (Description.contains("Invalid Order Numder with Special Characters")) {
			return checkOrderErrorText.getText();
		} else if (Description.contains("Invalid Order Number with Characters")) {
			return checkOrderErrorText.getText();
		}else if (Description.contains("Invalid Order Number")) {
			return checkOrderErrorText.getText();
		}else if (Description.contains("Invalid Order Email")) {
			return requestedFormatEmail.getText();
		}else if (Description.contains("Invalid Billing ZIP code with Characters")) {
			return requestedFormatBillCode.getText();
		}else if (Description.contains("Invalid Billing ZIP code")) {
			return checkOrderErrorText.getText();
		}else if (Description.contains("Invalid Empty Order Number")) {
			return orderNumberError.getText();
		}else if (Description.contains("Invalid Empty Order Email")) {
			return orderEmailError.getText();
		}else if (Description.contains("Invalid Empty Billing ZIP code")) {
			return billCodeErrror.getText();
		}else if (Description.contains("Invalid Billing ZIP code with Special Characters")) {
			return requestedFormatBillCode.getText();
		}
		return null;
	}
	
	//Login Method for Invalid	
	public String getErrorLoginData(String Description) {
		if(Description.contains("Invalid Both username and password")) {
			return errorMsgLongin2.getText();
		}else if(Description.contains("Invalid login with empty password")) {
			return errorMsgLogin1.getText();
		}else if (Description.contains("Invalid login with empty username")) {
			return errorMsgLogin1.getText();
		}else if (Description.contains("Invalid login with unregistered user")) {
			return errorMsgLogin1.getText();
		}else if (Description.contains("Invalid Login with Valid username and Invalid password")) {
			return errorMsgLogin1.getText();
		}
		return null;
	}
	
	// Login Method for Valid
	public String getLoginData() {
		return dashboard.getText();
	}
	
   //Logout Method
	public LoginPage Logout(){

		mouseoverElementAndClick(driver, lp_actualUserName, By.xpath("//a[@href='/on/demandware.store/Sites-RefArch-Site/en_US/Login-Logout']"));
		wait.until(ExpectedConditions.visibilityOf(lp_popUp));  
		clickElement(driver, lp_popUp);
		sleepinSeconds(2);
		return new LoginPage(driver);
	}




}
