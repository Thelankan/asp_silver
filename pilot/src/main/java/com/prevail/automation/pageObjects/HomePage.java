package com.prevail.automation.pageObjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.prevail.automation.Actions.SeleniumActions;

public class HomePage extends SeleniumActions{
	
    private static Logger Log = Logger.getLogger(HomePage.class);
	private WebDriver driver;
	private WebDriverWait wait;
	
	
		
	@FindBy(xpath = "//form[@name='simpleSearch']//input[@id='q']")
    private WebElement txtSearch;
	
	@FindBy(xpath = "//form[@name='simpleSearch']//button[@type='submit']")
    private WebElement btnSearch;
	
	@FindBy(xpath = "//span[@class='user-message']")
	private WebElement loginButton;
	
	@FindBy(xpath = "//*[@id=\"consent-tracking\"]/div/div/div[3]/div/button[1]")
	 private WebElement popUp;
	
	

	
	//Constructor
	public HomePage(WebDriver driver)
	{
		Log.info("Home Page constructor is Invoked");
		this.driver = driver;
		wait = new WebDriverWait(driver, 120);
		PageFactory.initElements(driver, this);
		wait.until(ExpectedConditions.titleContains("Salesforce Commerce Cloud"));
		wait.until(ExpectedConditions.visibilityOf(loginButton));

		
	}
	
	public void PopUp()
	{
		wait.until(ExpectedConditions.visibilityOf(popUp));
        clickElement(driver, popUp); 	
	}
	
	/*//Options - Womens, Mens, Electronics
	public NewArrivalsPage navigateToNewArrivals(String options) throws Exception 
	{
		By subMenuXpath = By.xpath("//ul[contains(@class,'menu-category')]/li/a[contains(.,'New Arrivals')]/following-sibling::div//a[contains(.,'"+ options +"')]");
		mouseoverElementAndClick(driver, menuNewArrivals, subMenuXpath);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='cat-banner']//h1[text()='"+ options +"']")));
		
		return new NewArrivalsPage(driver);
	}
	
	public SearchResultsPage searchItem(String searchtext)
	{
		setText(driver, txtSearch, searchtext);
		clickElement(driver, btnSearch);
		
		return new SearchResultsPage(driver);
	}*/
}