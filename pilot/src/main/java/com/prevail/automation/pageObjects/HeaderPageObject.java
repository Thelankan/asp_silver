package com.prevail.automation.pageObjects;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.prevail.automation.Actions.SeleniumActions;
import com.prevail.automation.Utility.FetchDataUtility;

public class HeaderPageObject extends SeleniumActions {

	private static Logger Log = Logger.getLogger(HeaderPageObject.class);
	private WebDriver driver;
	private WebDriverWait wait;
	private String file = System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx";
	private String sheetName = "HeaderMenu";

	@FindBy(xpath = "//span[@class='user-message']")
	private WebElement loginButton;

	@FindBy(xpath = "//input[@class='form-control search-field']")
	private WebElement searchTextBox;

	@FindBy(className = "fa fa-search")
	private WebElement searchButton;

	@FindBy(xpath = "//div//a[@class='minicart-link']")
	private WebElement miniCartIcon;

	@FindBy(xpath = "//ul[@class='nav navbar-nav']/li/a")
	private WebElement category1;

	@FindBy(xpath = "//ul[@class='nav navbar-nav']/li[2]/a")
	private WebElement category2;

	@FindBy(xpath = "//ul[@class='nav navbar-nav']/li[3]/a")
	private WebElement category3;

	@FindBy(xpath = "//ul[@class='nav navbar-nav']/li[4]/a")
	private WebElement category4;

	@FindBy(linkText = "Top Sellers")
	private WebElement category5;

	@FindBy(xpath = "//html[@id='ext-gen45']//div[@class='container cart']")
	private WebElement containerCart;

	@FindBy(xpath = "//div[@class='line-item-name']/span")
	private WebElement productinCart;

	// Constructor
	public HeaderPageObject(WebDriver driver) {
		Log.info("Header is Invoked");
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		PageFactory.initElements(driver, this);
		wait.until(ExpectedConditions.visibilityOf(loginButton));
	}

	// method to retrieve the categories in header
	public List<WebElement> Retrievecategory() {
		return driver.findElements(By.xpath("//*[@class='nav navbar-nav'][@role='menu']/li"));
	}
	//#sg-navbar-collapse > div > div > nav > div.menu-group > ul > li:nth-child(1)
	public List<WebElement> retrieveLevel1Categories(String categoryname) throws Exception {
		if (categoryname.equals(FetchDataUtility.getCellData(file, sheetName, 2,2))) {
			return RetrieveLevel1categories(category1);
		} else if (categoryname.equals(FetchDataUtility.getCellData(file, sheetName, 3,2))) {
			return RetrieveLevel1categories(category2);
		} else if (categoryname.equals(FetchDataUtility.getCellData(file, sheetName, 4,2))) {
			return RetrieveLevel1categories(category3);
		} else if (categoryname.equals(FetchDataUtility.getCellData(file, sheetName, 5,2))) {
			return RetrieveLevel1categories(category4);
		}
		return null;
	}

	// method to retrieve level1/sub categories
	public List<WebElement> RetrieveLevel1categories(WebElement element) throws InterruptedException {
		mouseoverElement(driver, element);
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='dropdown-menu show']/li"));
		return elements;
	}

	// method to retrieve level2/sub-sub categories
	public List<WebElement> RetrieveLevel2Categories(WebElement element) throws InterruptedException {
		mouseoverElement(driver, element);
		List<WebElement> elements = driver
				.findElements(By.xpath("//ul[@class='dropdown-menu show']/li[@class='dropdown-item']"));
		return elements;

	}

	public LoginPage clickLoginLink() {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(loginButton));
		clickElement(driver, loginButton);
		waitForPageLoad(driver);
		LoginPage loginPage = new LoginPage(driver);
		return new LoginPage(driver);
	}

	public boolean isElementDisplayed() {
		return isElementDisplayed(loginButton);
	}

	public boolean isSearchBoxDisplayed() {
		return isElementDisplayed(searchTextBox);
	}

	public boolean isMiniCartDisplayed() {
		return isElementDisplayed(miniCartIcon);
	}

	public String mouseOverMiniCart() {

		sleepinSeconds(5);
		mouseoverElement(driver, miniCartIcon);
		wait.until(ExpectedConditions.visibilityOf(productinCart));
		return getText(driver, productinCart);
	}

	public void clickOnMiniCart() {

		clickElement(driver, miniCartIcon);

	}

	public void clickOnTopSeller() {

		clickElement(driver, category5);

	}
	
	public List<WebElement> mouseOverMiniCartAndGetList(String locator) {
		sleepinSeconds(3);
		mouseoverElement(driver, miniCartIcon);
		sleepinSeconds(5);
		wait.until(ExpectedConditions.visibilityOf(productinCart));
		List<WebElement> list = getList(driver, locator);
		return list;
	}
	
	

}
