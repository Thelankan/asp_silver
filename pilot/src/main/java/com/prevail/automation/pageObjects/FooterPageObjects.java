package com.prevail.automation.pageObjects;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.prevail.automation.Actions.SeleniumActions;

public class FooterPageObjects extends SeleniumActions {
	private static Logger Log = Logger.getLogger(FooterPageObjects.class);
	private WebDriver driver;
	private WebDriverWait wait;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[1]/div/a/h3")
	private WebElement footerSection1;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[1]/div/span")
	private WebElement footerSection1Content;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[2]/div/h3/a")
	private WebElement footerSection2;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[2]/div/ul/li[1]/a")
	private WebElement footerSection2Link1;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[2]/div/ul/li[2]/a")
	private WebElement footerSection2Link2;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[2]/div/ul/li[3]/a")
	private WebElement footerSection2Link3;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[2]/div/ul/li[4]/a")
	private WebElement footerSection2Link4;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[3]/div/h3/a")
	private WebElement footerSection3;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[3]/div/ul/li[1]/a")
	private WebElement footerSection3Link1;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[3]/div/ul/li[2]/a")
	private WebElement footerSection3Link2;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[3]/div/ul/li[3]/a")
	private WebElement footerSection3Link3;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[3]/div/ul/li[4]/a")
	private WebElement footerSection3Link4;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[4]/div/h3/a")
	private WebElement footerSection4;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[4]/div/ul/li[1]/a")
	private WebElement footerSection4Link1;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[4]/div/ul/li[2]/a")
	private WebElement footerSection4Link2;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[4]/div/ul/li[3]/a")
	private WebElement footerSection4Link3;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[1]/div[4]/div/ul/li[4]/a")
	private WebElement footerSection4Link4;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[2]/div[1]/ul/li[1]/a")
	private WebElement footerSocialElement1;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[2]/div[1]/ul/li[2]/a")
	private WebElement footerSocialElement2;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[2]/div[1]/ul/li[3]/a")
	private WebElement footerSocialElement3;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[2]/div[1]/ul/li[4]/a")
	private WebElement footerSocialElement4;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[2]/div[1]/button/span/i[2]")
	private WebElement arrowupLink;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[2]/div[2]/div/div[1]")
	private WebElement copyrightText;

	@FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/div[1]/footer/div/div[2]/div[2]/div/div[2]")
	private WebElement postscriptTest;

	// Constructor
	public FooterPageObjects(WebDriver driver) {
		Log.info("Home Page constructor is Invoked");
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		PageFactory.initElements(driver, this);
		wait.until(ExpectedConditions.visibilityOf(footerSection1Content));
	}

	public void clickSocialNetworks(String socialElement) {
		if (socialElement.equalsIgnoreCase("LinkedIn")) {
			footerSocialElement1.click();
			waitForPageLoad(driver);
		} else if (socialElement.equalsIgnoreCase("Facebook")) {
			footerSocialElement2.click();
			waitForPageLoad(driver);
		} else if (socialElement.equalsIgnoreCase("Twitter")) {
			footerSocialElement3.click();
			waitForPageLoad(driver);
		} else if (socialElement.equalsIgnoreCase("YouTube")) {
			footerSocialElement4.click();
			waitForPageLoad(driver);
		}
	}
	
	public String getLinktext(String link) {
		if (link.equals("My Account")) {
			return footerSection2Link1.getText();
		} else if (link.equals("Check Order")) {
			return footerSection2Link2.getText();
		} else if (link.equals("Wish List")) {
			return footerSection2Link3.getText();
		} else if (link.equals("Gift Registry")) {
			return footerSection2Link4.getText();
		} else if (link.equals("Contact Us")) {
			return footerSection3Link1.getText();
		} else if (link.equals("Gift Certificates")) {
			return footerSection3Link2.getText();
		} else if (link.equals("Help")) {
			return footerSection3Link3.getText();
		} else if (link.equals("Site Map")) {
			return footerSection3Link4.getText();
		} else if (link.equals("About Us")) {
			return footerSection4Link1.getText();
		} else if (link.equals("Privacy")) {
			return footerSection4Link2.getText();
		} else if (link.equals("Terms")) {
			return footerSection4Link3.getText();
		} else if (link.equals("Jobs")) {
			return footerSection4Link4.getText();
		}
		return null;
	}

	public boolean verifyFooterLinkClickable(String link) {
		if (link.equals("My Account")) {
			return isElementClickable(footerSection2Link1);
		} else if (link.equals("Check Order")) {
			return isElementClickable(footerSection2Link2);
		} else if (link.equals("Wish List")) {
			return isElementClickable(footerSection2Link3);
		} else if (link.equals("Gift Registry")) {
			return isElementClickable(footerSection2Link4);
		} else if (link.equals("Contact Us")) {
			return isElementClickable(footerSection3Link1);
		} else if (link.equals("Gift Certificates")) {
			return isElementClickable(footerSection3Link2);
		} else if (link.equals("Help")) {
			return isElementClickable(footerSection3Link3);
		} else if (link.equals("Site Map")) {
			return isElementClickable(footerSection3Link4);
		} else if (link.equals("About Us")) {
			return isElementClickable(footerSection4Link1);
		} else if (link.equals("Privacy")) {
			return isElementClickable(footerSection4Link2);
		} else if (link.equals("Terms")) {
			return isElementClickable(footerSection4Link3);
		} else if (link.equals("Jobs")) {
			return isElementClickable(footerSection4Link4);
		}
		return false;
	}

	public String clickfooterSection1() {
		return footerSection1Content.getText();
	}

	public void clickfooterSection2(String footerSection2Element) {
		if (footerSection2Element.equalsIgnoreCase("My Account")) {
			footerSection2Link1.click();
			waitForPageLoad(driver);
		} else if (footerSection2Element.equalsIgnoreCase("Check Order")) {
			footerSection2Link2.click();
			waitForPageLoad(driver);
		} else if (footerSection2Element.equalsIgnoreCase("Wish List")) {
			footerSection2Link3.click();
			waitForPageLoad(driver);
		} else if (footerSection2Element.equalsIgnoreCase("Gift Registry")) {
			footerSection2Link4.click();
			waitForPageLoad(driver);
		}
	}

	public void clickfooterSection3(String footerSection3Element) {
		if (footerSection3Element.equalsIgnoreCase("Contact Us")) {
			footerSection3Link1.click();
			waitForPageLoad(driver);
		} else if (footerSection3Element.equalsIgnoreCase("Gift Certificates")) {
			footerSection3Link2.click();
			waitForPageLoad(driver);
		} else if (footerSection3Element.equalsIgnoreCase("Help")) {
			footerSection3Link3.click();
			waitForPageLoad(driver);
		} else if (footerSection3Element.equalsIgnoreCase("Site Map")) {
			footerSection3Link4.click();
			waitForPageLoad(driver);
		}
	}

	public void clickfooterSection4(String footerSection4Element) {
		if (footerSection4Element.equalsIgnoreCase("About Us")) {
			footerSection4Link1.click();
			waitForPageLoad(driver);
		} else if (footerSection4Element.equalsIgnoreCase("Privacy")) {
			footerSection4Link2.click();
			waitForPageLoad(driver);
		} else if (footerSection4Element.equalsIgnoreCase("Terms")) {
			footerSection4Link3.click();
			waitForPageLoad(driver);
		} else if (footerSection4Element.equalsIgnoreCase("Jobs")) {
			footerSection4Link4.click();
			waitForPageLoad(driver);
		}
	}

	public List<String> getFooterHeaderNames() {
		List<String> footerHeaderNames = new ArrayList<String>();
		footerHeaderNames.add(footerSection1.getText());
		footerHeaderNames.add(footerSection2.getText());
		footerHeaderNames.add(footerSection3.getText());
		footerHeaderNames.add(footerSection4.getText());
		return footerHeaderNames;
	}

	public List<String> getCopyRightScripts() {
		List<String> copyRightScripts = new ArrayList<String>();
		copyRightScripts.add(copyrightText.getText());
		copyRightScripts.add(postscriptTest.getText());
		return copyRightScripts;
	}
	
	public void clickCheckOrder(){
		 
		footerSection2Link2.click();
		sleepinSeconds(3);	
}

}
