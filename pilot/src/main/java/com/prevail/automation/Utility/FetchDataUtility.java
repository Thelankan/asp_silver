package com.prevail.automation.Utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.prevail.automation.Actions.SeleniumActions;

public class FetchDataUtility {

	private static Logger Log = Logger.getLogger(SeleniumActions.class);
	private WebDriverWait wait;
	protected static XSSFSheet ExcelWSheet;
	protected static XSSFWorkbook ExcelWBook;
	protected static XSSFRow Row;
	protected static XSSFCell Cell;

	public static Object[][] getTableArray(String FilePath, String SheetName) throws Exception {
		Object[][] tabArray = null;
		try {
			XSSFSheet Sheet = getExcelSheet(FilePath, SheetName);
			int startRow = 1;
			int startCol = 1;
			int ci, cj;
			int totalRows = Sheet.getLastRowNum();
			Row = Sheet.getRow(0);
			int totalCols = Row.getLastCellNum();
			tabArray = new String[totalRows][totalCols];
			ci = 0;
			for (int i = startRow; i <= totalRows; i++, ci++) {
				cj = 0;
				for (int j = startCol - 1; j < totalCols; j++, cj++) {
					tabArray[ci][cj] = getCellData(Sheet, i, j);
					System.out.println(tabArray[ci][cj]);

				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Could not read the Excel sheet");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Could not read the Excel sheet");
			e.printStackTrace();
		}
		return (tabArray);
	}

	@SuppressWarnings("deprecation")
	private static String getCellData(XSSFSheet sheet,int RowNum, int ColNum) throws Exception {
		try {
			Cell = sheet.getRow(RowNum).getCell(ColNum);

			if (Cell.getCellTypeEnum() == CellType.STRING)
				return Cell.getStringCellValue();
			else if (Cell.getCellTypeEnum() == CellType.NUMERIC || Cell.getCellTypeEnum() == CellType.FORMULA) {
				String cellValue = String.valueOf(Cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(Cell)) {
					DateFormat df = new SimpleDateFormat("dd/MM/yy");
					Date date = Cell.getDateCellValue();
					cellValue = df.format(date);
				}
				return cellValue;
			} else if (Cell.getCellTypeEnum() == CellType.BLANK)
				return "";
			else
				return String.valueOf(Cell.getBooleanCellValue());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw (e);
		}
	}

	public static List<String> getColumnData(String filePath, String sheetName, String coulumnName) throws Exception {
		List<String> testArray = new ArrayList<String>();

		int expColumn = 0;
		XSSFSheet sheet = getExcelSheet(filePath, sheetName);
		XSSFRow row = sheet.getRow(0);
		for (int i = 0; i < row.getLastCellNum(); i++) {
			if (row.getCell(i) != null) {
				if (row.getCell(i).toString().toLowerCase().equals(coulumnName.toLowerCase())) {
					expColumn = i;
					break;
				}
			}
		}
		if(sheet.getRow(0).getCell(expColumn).toString().equals(coulumnName)) {
		System.out.println("Expected Column"+sheet.getRow(0).getCell(expColumn).toString());
		try {
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				XSSFCell Cell = sheet.getRow(i).getCell(expColumn);
				testArray.add(Cell.toString());
			}
			return testArray;
		} catch (Exception NullPointerException) {
			return testArray;
		}
		}

		return null;
	}

	public static String getCellData(String filePath, String sheetName, int rowNum, int cellNum) throws Exception {
		String data = null;
		try {
			XSSFRow r = getExcelSheet(filePath, sheetName).getRow(rowNum);
			data = r.getCell(cellNum).toString();
		} catch (NullPointerException e) {
			System.out.println(".....please verify the rowNumber and cellNumber...........");
			throw e;
		}
		return data;
	}

	public static List<String> getRowData(String filePath, String sheetName, int rowNumber) throws Exception {
		List<String> rowData = new ArrayList<String>();
		try {

			XSSFRow r = getExcelSheet(filePath, sheetName).getRow(rowNumber);
			for (Cell cell : r) {
				rowData.add(cell.toString());
			}
		} catch (NullPointerException e) {
			System.out.println("..........please verify the rowNumber.........");
			throw e;
		}
		return rowData;

	}

	public static ArrayList<String[]> getMultipleRowsData(String filePath, String sheetName, int startRowNumber,
			int endRowNumber) throws Exception {
		ArrayList<String[]> rw = new ArrayList<String[]>();
		try {
			String[] temp = null;
			XSSFSheet sheet = getExcelSheet(filePath, sheetName);
			int ci = 0;
			for (int i = startRowNumber; i <= endRowNumber; i++, ci++) {
				temp = new String[sheet.getRow(i).getLastCellNum()];
				XSSFRow r = sheet.getRow(i);
				for (int j = 0; j < sheet.getRow(i).getLastCellNum(); j++) {
					temp[j] = r.getCell(j).toString();
				}
				rw.add(temp);
			}
		} catch (NullPointerException e) {
			System.out.println("........please verify the startRowNumber and lastRowNumber........");
			throw e;
		}

		return rw;
	}
	
	
	//to support Qtest this method ignores testcase no and description field
	public static List<String> getRowWithCellValue(String filePath, String sheetName,String cellValue) throws Exception{
		List<String> rowData=new ArrayList<String>();
		try {		
		XSSFSheet sheet = getExcelSheet(filePath, sheetName);
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			XSSFRow row = sheet.getRow(i);
			if(row.getCell(2).toString().toLowerCase().equals(cellValue.toLowerCase())) {
				for(int j=3;j<row.getLastCellNum();j++) {
					rowData.add(row.getCell(j).toString());
				}		
			break;
			}
		}
		return rowData;
		}
		catch(NullPointerException e) {
			return rowData;
		}	
	}
	
	public static String[] getRowSplitData(String filePath, String sheetName,String cellValue) throws Exception{
		String[] rowData=null;
		try {		
		XSSFSheet sheet = getExcelSheet(filePath, sheetName);
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			XSSFRow row = sheet.getRow(i);
			if(row.getCell(2).toString().toLowerCase().equals(cellValue.toLowerCase())) {
			 rowData=row.getCell(3).toString().split(",");			
			break;
			}
		}
		return rowData;
		}
		catch(NullPointerException e) {
			return rowData;
		}	
		}	
	
	public static XSSFSheet getExcelSheet(String filePath, String sheetName) throws Exception {
		FileInputStream ExcelFile = new FileInputStream(filePath);
		XSSFWorkbook ExcelWBook = new XSSFWorkbook(ExcelFile);
		XSSFSheet ExcelWSheet = ExcelWBook.getSheet(sheetName);
		return ExcelWSheet;
	}
}
