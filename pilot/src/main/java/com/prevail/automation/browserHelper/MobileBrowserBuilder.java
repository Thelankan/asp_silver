package com.prevail.automation.browserHelper;

import org.openqa.selenium.WebDriver;

public interface MobileBrowserBuilder {
	   
	  public void createMobileBrowserDriver(String browser, String url);
	  
	  public WebDriver getDriver();
	  
	  public void quitDriver();
	}