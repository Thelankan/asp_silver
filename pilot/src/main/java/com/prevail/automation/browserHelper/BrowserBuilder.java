package com.prevail.automation.browserHelper;

import java.io.File;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.prevail.automation.Listeners.WebDriverListener;
import com.prevail.automation.Utility.ReadConfigFile;
import com.prevail.automation.pageObjects.HomePage;

public class BrowserBuilder extends ReadConfigFile implements LocalBrowserBuilder, MobileBrowserBuilder, RemoteBrowserBuilder{

	private static Logger Log = Logger.getLogger(LocalBrowserBuilder.class);
	private WebDriver basedriver;
	public WebDriver driver;
	protected HomePage homepage;
	private static final String ESCAPE_PROPERTY = "org.uncommons.reportng.escape-output";
	DesiredCapabilities capabilities;

	

	@Parameters({"browser", "url"})
	@BeforeClass
	public void createLocalBrowserDriver(String browser, String url) {
		//readProperties();
		System.setProperty(ESCAPE_PROPERTY, "false");

		if(browser.toLowerCase().contains("chrome")){
			capabilities = DesiredCapabilities.chrome();

			ChromeDriverService service = new ChromeDriverService.Builder()
					.usingDriverExecutable(new File("src/test/resources/driver/chromedriver.exe"))
					.usingAnyFreePort()
					.build();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			options.setCapability("CapabilityType.ACCEPT_SSL_CERTS", true);
			options.addArguments("--start-maximized");
			options.merge(capabilities);    
			basedriver = new ChromeDriver(service, options);
		}
		
		else if(browser.toLowerCase().contains("firefox")){
			capabilities = DesiredCapabilities.firefox();
			// FirefoxDriverService service = new FirefoxDriverService();
			FirefoxOptions options = new FirefoxOptions();
			options.setAcceptInsecureCerts(true);
			DesiredCapabilities cap=new DesiredCapabilities();
			cap.setCapability(CapabilityType.HAS_NATIVE_EVENTS, false);
			cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			System.setProperty("webdriver.gecko.driver", "src/test/resources/driver/geckodriver.exe");
			options.addArguments("--start-maximized");
			basedriver = new FirefoxDriver(options);
		}
		
		else if(browser.toLowerCase().contains("ie")){

			InternetExplorerOptions options = new InternetExplorerOptions();

			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			System.setProperty("webdriver.ie.driver", "src/test/resources/driver/IEDriverServer.exe");
			basedriver = new InternetExplorerDriver(capabilities);

			basedriver.manage().window().maximize();		
		}

		EventFiringWebDriver efwd=new EventFiringWebDriver(basedriver);
		WebDriverListener eventListener=new WebDriverListener(basedriver);
		efwd.register(eventListener);
		driver=efwd;

		driver.manage().deleteAllCookies();
		driver.get(url);
		if(browser.toLowerCase().contains("ie"))
			driver.findElement(By.partialLinkText("Continue to this website")).click();
	}

	
	
	
	public WebDriver getDriver(){
		
		return driver;
	}

	
	

	@AfterClass()
	public void quitDriver() {
		
		if(driver!=null){
			try{
				driver.close();
			}
			catch(WebDriverException e){
				System.out.println("**********CAUGHT EXCEPTION IN DRIVER TEAR DOWN**********");
				System.out.println(e);
			}
		}
	}

	
	
	@Override
	public void createRemoteBrowserDriver(String browser, String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createMobileBrowserDriver(String browser, String url) {
		// TODO Auto-generated method stub

	}



}
