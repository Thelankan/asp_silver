package com.prevail.automation.browserHelper;

import org.openqa.selenium.WebDriver;

public interface RemoteBrowserBuilder {
	   
	  public void createRemoteBrowserDriver(String browser, String url);
	  
	  public WebDriver getDriver();
	  
	  public void quitDriver();
	}