package com.prevail.automation.browserHelper;


import org.openqa.selenium.WebDriver;


public interface LocalBrowserBuilder
{
   
  public void createLocalBrowserDriver(String browser, String url);
  
  public WebDriver getDriver();
  
  public void quitDriver();
}