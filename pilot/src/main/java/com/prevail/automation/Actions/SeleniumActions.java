package com.prevail.automation.Actions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.swing.text.Document;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumActions {

	private static Logger Log = Logger.getLogger(SeleniumActions.class);
	private WebDriverWait wait;
	protected static XSSFSheet ExcelWSheet;
	protected static XSSFWorkbook ExcelWBook;
	protected static XSSFRow Row;
	protected static XSSFCell Cell;

	public void clickElement(WebDriver driver, WebElement element) {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	public boolean isElementClickable(WebElement element) {
		return element!=null && element.isDisplayed() && element.isEnabled() ? true : false;		

	}

	public void clickElementByLocator(WebDriver driver, By bylocator) {
		wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(bylocator));
		wait.until(ExpectedConditions.elementToBeClickable(bylocator));
		// driver.findElement(bylocator).click();
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(bylocator)).click().build().perform();
	}

	// 1
	public void mouseoverElement(WebDriver driver, WebElement element) {
		wait = new WebDriverWait(driver, 60);
		JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
		wait.until(ExpectedConditions.visibilityOf(element));
		Actions a = new Actions(driver);
		a.moveToElement(element).build().perform();
	}

	// 2
	public boolean isElementDisplayed(WebElement element) {
		try {
			if (element.isEnabled() && element.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			System.out.println(e + "is cought for" + element);
		}
		return false;
	}

	//3
	public static void waitForPageLoad(WebDriver driver) {
		boolean pageLoaded;
		for (int tries = 0; tries < 5; tries++) {
			if (((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete")) {
				break;
			};
		}
	}

	// 4
	/*
	 * public void isElementClickable(WebDriver driver,WebElement element) {
	 * ((JavascriptExecutor)(driver)).executeScript(Document.vie, arg1) }
	 */

	public void mouseoverElementAndClick(WebDriver driver, WebElement element, By locator) {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		Actions a = new Actions(driver);
		a.moveToElement(element).build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		a.moveToElement(driver.findElement(locator)).click().build().perform();
	}

	public void setText(WebDriver driver, WebElement element, String text) {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.clear();
		element.sendKeys(text);
	}

	public void sleepinSeconds(int sec) {
		try {
			Log.info("Sleeping for " + sec + " seconds");
			Thread.sleep(sec * 1000);
		} catch (Exception e) {
			Log.info("Problem in sleep");
		}
	}

	public void waitForSpinner(WebDriver driver) {
		try {
			wait = new WebDriverWait(driver, 20);
			By spinner = By.xpath("//div[contains(@class,'loading-indicator')]");
			wait.until(ExpectedConditions.visibilityOfElementLocated(spinner));
			wait.until(ExpectedConditions.invisibilityOfElementLocated(spinner));
		} catch (Exception e) {
			// No spinner
		}
	}

	protected boolean isAtleastOneElementDisplayed(WebDriver driver, By by) {
		List<WebElement> elements = driver.findElements(by);
		for (WebElement element : elements) {
			if (element.isDisplayed())
				return true;
		}
		return false;
	}

	protected boolean isAtleastOneElementDisplayed(WebDriver driver, List<WebElement> elements) {
		for (WebElement element : elements) {
			if (element.isDisplayed())
				return true;
		}
		return false;
	}

	public void waitForSpinner1(WebDriver driver) {
		By spinner = By.xpath("//div[contains(@class,'loading-indicator')]");
		waitForSpin(driver, spinner, 1);
	}

	public boolean waitForSpin(WebDriver driver, By waitspinnerloc, int maxwaitminutes) {
		boolean iswaitingComplete = false;
		int sleepTimeSeconds = 3;
		int maxNumberofTrials = maxwaitminutes * 60 / sleepTimeSeconds;
		Log.info("Waiting for loading bar/spinner ...");
		Log.debug("Sleeping... maxTimeout=" + maxwaitminutes + " minutes.");
		try {
			for (int i = 0; !iswaitingComplete && i < maxNumberofTrials; i++) {
				try {
					Thread.sleep(sleepTimeSeconds);
				} catch (Exception e) {
				}

				try {
					iswaitingComplete = !this.isAtleastOneElementDisplayed(driver, waitspinnerloc);
				} catch (StaleElementReferenceException e) {
					iswaitingComplete = !this.isAtleastOneElementDisplayed(driver, waitspinnerloc);
				} catch (Exception e) {
					Log.info("Failed to find atleast one element");
				}
			}
		} catch (StaleElementReferenceException e) {
			iswaitingComplete = true;
		}
		if (!iswaitingComplete) {
			Log.warn("Waited for " + maxwaitminutes + " minutes. But still the operation/load is in progress.");
			return false;
		}
		Log.info("Loading Spinner is closed.");
		return true;
	}


	public String getText(WebDriver driver, WebElement element) {

		wait = new WebDriverWait(driver, 60);
		String text = element.getText();
		return text;
	}

	public List<WebElement> getList(WebDriver driver, String productinCart) {
		wait = new WebDriverWait(driver, 60);
		List<WebElement> list = driver.findElements(By.xpath(productinCart));
		return list;
	}
	
	public void selectElement(WebDriver driver, WebElement element, String text) {
        wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOf(element));
        Select select = new Select(element);
        select.selectByVisibleText(text);
}
	
	


}