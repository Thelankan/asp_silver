package com.prevail.automation.Actions;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SeleniumCSSutility  {


	public static void textColor(WebDriver driver, By locator)
	{
		String TextColor= driver.findElement(locator).getCssValue("color");
		String Text= driver.findElement(locator).getText();
		System.out.println( "Text color of "+Text + TextColor);
	}


	public static void BackgroundColor(WebDriver driver, By locator)
	{ 
		String backgroundColor = driver.findElement(locator).getCssValue("background-color");
		System.out.println("backgroundColor :"+ backgroundColor);
	}


	public static void FontSize(WebDriver driver, By locator)
	{
		String FontSize = driver.findElement(locator).getCssValue("font-Size");
		System.out.println("Font Size :"+ FontSize);
	}


	public static void FontFamily(WebDriver driver, By locator)
	{
		String FontFamily = driver.findElement(locator).getCssValue("font-Family");
		System.out.println("Font Family :"+ FontFamily);
	}  


	public static void FontWeight(WebDriver driver, By locator)
	{ 
		String FontWeight = driver.findElement(locator).getCssValue("font-Weight");
		System.out.println("Font Weight :"+ FontWeight);
	} 



	public static void PaddingTop(WebDriver driver, By locator)
	{ 
		String PaddingTop = driver.findElement(locator).getCssValue("Padding-Top");
		System.out.println("Padding_Top :"+ PaddingTop);
	}


	public static void Paddingbottom(WebDriver driver, By locator)
	{ 
		String PaddingBottom = driver.findElement(locator).getCssValue("Padding-Bottom");
		System.out.println("Padding_Bottom :"+ PaddingBottom);
	}


	public static void width(WebDriver driver, By locator)
	{ 
		String width = driver.findElement(locator).getCssValue("width");
		System.out.println("width :"+ width);
	}


	public static void height(WebDriver driver, By locator)
	{ 
		String height = driver.findElement(locator).getCssValue("height");
		System.out.println("height :"+ height);
	}  


	public static void alignment(WebDriver driver, By locator)
	{ 
		String alignment = driver.findElement(locator).getCssValue("text-align");
		System.out.println("alignment :"+ alignment);
	}


	public static void textDecoration(WebDriver driver, By locator)
	{
		String textDecorationStyle = driver.findElement(locator).getCssValue("text-decoration-style");
		System.out.println("textDecorationStyle :"+ textDecorationStyle);
		String textDecorationLine = driver.findElement(locator).getCssValue("text-decoration-line");
		System.out.println("textDecorationLine :"+ textDecorationLine);
		String textDecorationColor = driver.findElement(locator).getCssValue("text-decoration-color");
		System.out.println("textDecorationColor :"+ textDecorationColor);

	}  



	public static void margin(WebDriver driver, By locator)
	{
		String marginBottom = driver.findElement(locator).getCssValue("margin-bottom");
		System.out.println("marginBottom :"+ marginBottom);
		String marginTop = driver.findElement(locator).getCssValue("margin-Top");
		System.out.println("marginTop :"+ marginTop);
		String marginLeft = driver.findElement(locator).getCssValue("margin-Left");
		System.out.println("marginLeft :"+ marginLeft);
		String marginRight = driver.findElement(locator).getCssValue("margin-Right");
		System.out.println("marginRight :"+ marginRight);

	} 


	/**@Test 
    public void getAtribute()
    {
         String wb =driver.findElement(locator).getAttribute("values");
         System.out.println("Attribute is : " + wb);
    }  
	 **/


	public static void getImageDeminsion(WebDriver driver, By locator)
	{ //By.className("desktop-only")
		int width=driver.findElement(locator).getSize().getWidth();
		int height=driver.findElement(locator).getSize().getHeight();
		System.out.println("width is "+ width +" and height is " + height);
	}  


}


