package com.prevail.automation.Actions;

import java.awt.image.BufferedImage;

import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

public class ImageComparisionUtil {
	
public boolean compareImage(BufferedImage actual,BufferedImage expected) {
		
		ImageDiffer imgDiff=new ImageDiffer();
		ImageDiff diff=imgDiff.makeDiff(actual, expected);
		
		if(diff.hasDiff()) {
			return false;
		}
		return true;
		
	}
	
	public boolean compareImage(Screenshot actual,Screenshot expected) {
			
			ImageDiffer imgDiff=new ImageDiffer();
			ImageDiff diff=imgDiff.makeDiff(actual, expected);
			
			if(diff.hasDiff()) {
				return false;
			}
			return true;
			
		}
}
