package com.sitegenesis.test;

import static org.testng.AssertJUnit.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.prevail.automation.Actions.SeleniumActions;
import com.prevail.automation.Utility.FetchDataUtility;
import com.prevail.automation.browserHelper.BrowserBuilder;
import com.prevail.automation.browserHelper.LocalBrowserBuilder;
import com.prevail.automation.pageObjects.HeaderPageObject;
import com.prevail.automation.pageObjects.HomePage;
import com.prevail.automation.pageObjects.LoginPage;


public class LoginAndRegistrationTests extends BrowserBuilder{



	public HomePage homePage;
	public HeaderPageObject headerPage;
	public LoginPage LoginPage;

	
	@DataProvider(name = "ValidateLogin")
	public Object[][] ValidLogin() throws Exception{
		Object[][] testObjArray = FetchDataUtility.getTableArray(System.getProperty("user.dir")+"\\src\\test\\resources\\Testdata\\TestData.xlsx", "Login");
		return (testObjArray);
	}

	@DataProvider(name = "ValidateRegistration")
	public Object[][] ValidRegistration() throws Exception{
		Object[][] testObjArray = FetchDataUtility.getTableArray(System.getProperty("user.dir")+"\\src\\test\\resources\\Testdata\\TestData.xlsx", "Registration");
		return (testObjArray);
	}
	
	
	@BeforeClass
	public void setup(){

		homePage = new HomePage(driver);
		homePage.PopUp();
		headerPage=new HeaderPageObject(driver);
		sleepinSeconds(3);
		headerPage.clickLoginLink();
		LoginPage = new LoginPage(driver);
	}


	//-----------------------------------------------Login Section TestCases-------------------------------------------------------------------------------
	
	//Validate Login scenario's - both validate and invalid, pick dataset from Excel sheet 
	@Test(priority = 2, dataProvider="ValidateLogin")
	public void validateLogin(String Scenario, String User, String Pass, String Expected) throws Exception {

		LoginPage.login(User, Pass);
		if (Scenario.contains("Invalid_Login") )
			assertEquals(LoginPage.LoginPageTitle(),Expected);
		else if (Expected.contains("Valid_Login")){	
			assertEquals(LoginPage.LoginPageTitle(),Expected);
		}

	}
	
	
	//-----------------------------------------------Registration Section TestCases-------------------------------------------------------------------------------

	
	//Validate Registration scenario's - both validate and invalid, pick dataset from Excel sheet 
	@Test(priority = 2, dataProvider="ValidateRegistration")
	public void validateRegistration(String Scenario, String Fname , String Lname, String Phone, String Email, String ConfirmEmail, String Password, String ConfirmPassword, String Expected) throws Exception {

		LoginPage.RegistrationWithAddtoMailChecked(Fname, Lname, Phone, Email, ConfirmEmail, Password, ConfirmPassword);
		if (Scenario.contains("Invalid_Registration") )
			assertEquals(LoginPage.LoginPageTitle(),Expected);
		else if (Expected.contains("Valid_Registration")){	
			assertEquals(LoginPage.LoginPageTitle(),Expected);
		}

	}
	
	
	 

	@AfterMethod
	public void afterMethod(){

		if (LoginPage.LoginPageTitle().equalsIgnoreCase("Dashboard"))
		{
			LoginPage.Logout();
			sleepinSeconds(2);
			headerPage.clickLoginLink();
			LoginPage = new LoginPage(driver);

		}
	}



}
