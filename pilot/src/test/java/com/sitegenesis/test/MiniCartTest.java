package com.sitegenesis.test;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.prevail.automation.Utility.FetchDataUtility;
import com.prevail.automation.browserHelper.BrowserBuilder;
import com.prevail.automation.pageObjects.HeaderPageObject;
import com.prevail.automation.pageObjects.HomePage;
import com.prevail.automation.pageObjects.MiniCartObjects;

public class MiniCartTest extends BrowserBuilder {

	public HomePage homePage;
	public MiniCartObjects minicartobj;
	public HeaderPageObject headerPage;
	private String file = System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx";
	private String sheetName = "MiniCart";
	SoftAssert sa = new SoftAssert();

	@DataProvider(name = "MultipleProducts")
	public Iterator<String> subcategoriesTestData() throws Exception {
		List<String> testObjArray = FetchDataUtility.getColumnData(file, sheetName, "MiniCart Labels");
		Iterator<String> l = testObjArray.iterator();
		return (l);
	}

	@BeforeClass
	public void setup() {
		homePage = new HomePage(driver);
		homePage.PopUp();
		minicartobj = new MiniCartObjects(driver);
	}

	/**
	 * Test to add a product in Cart and verify if it is present in cart
	 * 
	 * @throws Exception
	 */

	@Test(priority = 1)
	public void verfyItemsInCart() throws Exception {
		minicartobj.addToCart();
		String expectedMsg = minicartobj.getMiniCartPopUpMsg();
		headerPage = new HeaderPageObject(driver);
		String proctName = headerPage.mouseOverMiniCart();
		List<String> expectedProductName = FetchDataUtility.getRowWithCellValue(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart",
				"SingleProduct");
		for (int i = 0; i < expectedProductName.size(); i++) {
			sa.assertTrue(proctName.contains(expectedProductName.get(i)),
					"Verified the cart does not contain " + expectedProductName.get(i));
			sa.assertEquals(expectedMsg, "Product added to basket", "Verified " + expectedMsg + "is not displayed");
			sa.assertEquals(minicartobj.miniCartProductNum(), "1",
					"Verified the number displayed on cart is incorrect");
			clickElement(driver, minicartobj.addToCart);
			sa.assertEquals(minicartobj.miniCartProductNum(), "2",
					"Verified the number displayed on cart is incorrect");
			sa.assertEquals(minicartobj.clickOnviewCart(), expectedProductName.get(i),
					"Verified the View Cart Page does not contains " + expectedProductName.get(i));
			break;
		}
		proctName = headerPage.mouseOverMiniCart();
		List<String> newArrivals = minicartobj.getMiniCartLabels();
		List<String> expectednewArrivals = FetchDataUtility.getRowWithCellValue(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart",
				"Labels");
		for (int i = 0; i < expectednewArrivals.size(); i++) {
			sa.assertEquals(newArrivals.get(i), expectednewArrivals.get(i));
		}
		minicartobj.emptyCart();
		String ExpectedText = minicartobj.miniCartProductNum();
		sa.assertEquals(ExpectedText, "0", "Verified the page does not displays " + ExpectedText);
		sa.assertAll();
	}

	/**
	 * Verifies the total of products, in which quantity is increased at different
	 * stage
	 * 
	 * @throws Exception
	 */
	@Test(priority = 13)
	public void addAndIncreaseQuantity() throws Exception {
		List<String> expectedProductName = FetchDataUtility.getRowWithCellValue(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart",
				"TwoProducts");
		sleepinSeconds(1);
		float expectedPrice = minicartobj.addAndIncreaseQauntty(expectedProductName);
		minicartobj.mouseOverMiniCart();
		String expectedprice = getText(driver, minicartobj.mncrt_estimated_total);
		float estimatePrice = minicartobj.convertPriceToFloat(expectedprice);
		sa.assertEquals(estimatePrice, expectedPrice,
				"Verified, Expected -" + estimatePrice + "does not match Actaul -" + expectedPrice);

		minicartobj.addAllToCart();
		// List<String> productPrice = minicartobj.addAlltocartNGetLabels();
		headerPage = new HeaderPageObject(driver);
		sleepinSeconds(1);
		List<WebElement> productName = headerPage.mouseOverMiniCartAndGetList("//div[@class='line-item-name']/span");
		List<String> expectedProductName1 = FetchDataUtility.getRowWithCellValue(
				System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx", "MiniCart",
				"MultipleProducts");
		sleepinSeconds(2);
		for (int p = 0; p < productName.size(); p++) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", productName.get(p));
			System.out.println("expectedProductName1 " + expectedProductName1);
			System.out.println("productName.get(p).getText()) " + productName.get(p).getText());
			sa.assertTrue(expectedProductName1.contains(productName.get(p).getText()));
		}

		sa.assertAll();

		/*
		 * for (int p = 0; p < expectedProductName.size(); p++) {
		 * minicartobj.emptyCart(); }
		 */
	}
}
