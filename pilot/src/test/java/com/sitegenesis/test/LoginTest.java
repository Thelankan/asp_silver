package com.sitegenesis.test;

import static org.testng.Assert.*;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.prevail.automation.Utility.FetchDataUtility;
import com.prevail.automation.browserHelper.BrowserBuilder;
import com.prevail.automation.pageObjects.FooterPageObjects;
import com.prevail.automation.pageObjects.HeaderPageObject;
import com.prevail.automation.pageObjects.HomePage;
import com.prevail.automation.pageObjects.LoginPage;

public class LoginTest extends BrowserBuilder {
	
	public LoginPage loginpage;
	public String file = System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx";
	public String sheet = "Login";
	public HeaderPageObject headerPage;
	
	@DataProvider(name = "Invalid Login")
	public Iterator<String[]> InvalidData() throws Exception {
		List<String[]> testObjArray = FetchDataUtility.getMultipleRowsData(file, sheet, 1, 5);
		Iterator<String[]> l = testObjArray.iterator();
		return (l);
	}
	
	
	@DataProvider(name = "Valid Login")
	public Iterator<String[]> validData() throws Exception {
		List<String[]> testObjArray = FetchDataUtility.getMultipleRowsData(file, sheet, 6, 8);
		Iterator<String[]> l = testObjArray.iterator();
		return (l);	
	}
	
	@BeforeClass
	public void setup() {
		homepage = new HomePage(driver);
		homepage.PopUp();
		headerPage=new HeaderPageObject(driver);
	}

	@BeforeMethod
	public void beforeTestMethod() {
		headerPage.clickLoginLink();
		loginpage = new LoginPage(driver);
	}
	
	@Test(priority = 1, dataProvider = "Invalid Login")
	public void verifyInvalidLogin( String Tc,String TestDescription, String Username,
			String Password, String ExceptedPageTitle, String ErrorValidation) throws Exception {
		loginpage.login(Username, Password);
		assertTrue(loginpage.getErrorLoginData(TestDescription).contains(ErrorValidation));
	}
	
	@Test(priority = 2, dataProvider = "Valid Login")
	public void verifySuccessfulLogin(String Tc, String TestDescription, String Username,
			String Password, String ExceptedPageTitle, String ErrorValidation) throws Exception {
		loginpage.login(Username, Password);
		assertEquals(loginpage.getLoginData(),ExceptedPageTitle);
		sleepinSeconds(3);
		loginpage.Logout();
	}
	
}
