package com.sitegenesis.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.fail;
import static org.testng.AssertJUnit.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.prevail.automation.Utility.FetchDataUtility;
import com.prevail.automation.browserHelper.BrowserBuilder;
import com.prevail.automation.pageObjects.FooterPageObjects;
import com.prevail.automation.pageObjects.HomePage;

public class FooterTest extends BrowserBuilder {
	public FooterPageObjects footerpageobject;
	public String file = System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx";
	public String sheet = "PilotFooter";

	@DataProvider(name = "Social Network")
	public Iterator<String> socialNetworkTestData() throws Exception {
		List<String> testObjArray = FetchDataUtility.getColumnData(file, sheet,
				FetchDataUtility.getCellData(file, sheet, 0, 8));
		Iterator<String> data = testObjArray.iterator();
		return (data);
	}

	@DataProvider(name = "Footer Section1")
	public Iterator<String> footerSection1TestData() throws Exception {
		List<String> testObjArray = FetchDataUtility.getColumnData(file, sheet,
				FetchDataUtility.getCellData(file, sheet, 0, 3));
		Iterator<String> l = testObjArray.iterator();
		return (l);
	}

	@DataProvider(name = "Footer Section2")
	public Iterator<String> footerSection2TestData() throws Exception {
		List<String> testObjArray = FetchDataUtility.getColumnData(file, sheet,
				FetchDataUtility.getCellData(file, sheet, 0, 4));
		Iterator<String> l = testObjArray.iterator();
		return (l);
	}

	@DataProvider(name = "Footer Section3")
	public Iterator<String> footerSection3TestData() throws Exception {
		List<String> testObjArray = FetchDataUtility.getColumnData(file, sheet,
				FetchDataUtility.getCellData(file, sheet, 0, 5));
		Iterator<String> l = testObjArray.iterator();
		return (l);
	}

	@DataProvider(name = "Footer Section4")
	public Iterator<String> footerSection4TestData() throws Exception {
		List<String> testObjArray = FetchDataUtility.getColumnData(file, sheet,
				FetchDataUtility.getCellData(file, sheet, 0, 6));
		Iterator<String> l = testObjArray.iterator();
		return (l);
	}

	@BeforeClass
	public void setup() {
		homepage = new HomePage(driver);
		homepage.PopUp();
		footerpageobject = new FooterPageObjects(driver);
	}

	@Test(priority = 1, dataProvider = "Social Network")
	public void verifySocialLink(String socialNetwork) {
		String socialNetworkWindow = null;
		String parentWindow = driver.getWindowHandle();
		footerpageobject.clickSocialNetworks(socialNetwork);
		Set<String> allWindows = driver.getWindowHandles();
		for (String string : allWindows) {
			if (string.contains(socialNetwork));
			socialNetworkWindow = string;
		}
		driver.switchTo().window(socialNetworkWindow);
		if (driver.getTitle().contains(socialNetwork)) {
			assertTrue(true);
		} else {
			driver.close();
			driver.switchTo().window(parentWindow);
			assertFalse(true, socialNetwork + "is not working");
		};
		driver.close();
		driver.switchTo().window(parentWindow);
	}

	@Test(priority = 2)
	public void verifyfooterHeaderLink() throws Exception {
		List<String> expected = FetchDataUtility.getColumnData(file, sheet,
				FetchDataUtility.getCellData(file, sheet, 0, 1));
		List<String> actual = footerpageobject.getFooterHeaderNames();
		for (int i = 0; i < expected.size(); i++) {
			assertEquals(actual.get(i), expected.get(i));
		}
	}

	@Test(priority = 3, dataProvider = "Footer Section1")
	public void verifyfooterSection1(String footerSection1) {
		assertEquals(footerpageobject.clickfooterSection1(), footerSection1.toString());
	}

	@Test(priority = 4, dataProvider = "Footer Section2")
	public void verifyfooterSection2(String footerSection2) {
		if (footerpageobject.verifyFooterLinkClickable(footerSection2)) {
			assertEquals(footerSection2, footerpageobject.getLinktext(footerSection2));
			footerpageobject.clickfooterSection2(footerSection2);
			driver.navigate().back();
			assertTrue(footerSection2 + "is clickable", true);
		} else {
			assertFalse(true, footerSection2 + "link is not clickable");
		}
	}

	@Test(priority = 5, dataProvider = "Footer Section3")
	public void verifyfooterSection3(String footerSection3) {
		if (footerpageobject.verifyFooterLinkClickable(footerSection3)) {
			assertEquals(footerSection3, footerpageobject.getLinktext(footerSection3));
			footerpageobject.clickfooterSection3(footerSection3);
			driver.navigate().back();
			assertTrue(footerSection3 + "is clickable", true);
		} else {
			assertFalse(true, footerSection3 + "link is not clickable");
		}
	}

	@Test(priority = 6, dataProvider = "Footer Section4")
	public void verifyfooterSection4(String footerSection4) {
		if (footerpageobject.verifyFooterLinkClickable(footerSection4)) {
			assertEquals(footerSection4, footerpageobject.getLinktext(footerSection4));
			footerpageobject.clickfooterSection4(footerSection4);
			driver.navigate().back();
			assertTrue(footerSection4 + "is clickable", true);
		} else {
			assertFalse(true, footerSection4 + "link is not clickable");
		}
	}

	@Test(priority = 7)
	public void verifyFooterCopyRightText() throws Exception {
		List<String> expected = FetchDataUtility.getColumnData(file, sheet,
				FetchDataUtility.getCellData(file, sheet, 0, 10));
		List<String> actual = footerpageobject.getCopyRightScripts();
		for (int i = 0; i < expected.size(); i++) {
			assertEquals(actual.get(i), expected.get(i));
		}
	}

}
