package com.sitegenesis.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.prevail.automation.Utility.FetchDataUtility;
import com.prevail.automation.browserHelper.BrowserBuilder;
import com.prevail.automation.pageObjects.HeaderPageObject;
import com.prevail.automation.pageObjects.HomePage;

public class HeaderTest extends BrowserBuilder {

	public HeaderPageObject headerPageObject;
	public HomePage homePage;
	private String file = System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx";
	private String sheetName = "HeaderMenu";

	@DataProvider(name = "level1")
	public String[] subcategoriesTestData() throws Exception {
		String[] testObjArray = FetchDataUtility.getRowSplitData(file, sheetName,
				FetchDataUtility.getCellData(file, sheetName, 1, 2));
		return (testObjArray);
	}

	@BeforeClass
	public void setup() {
		homepage = new HomePage(driver);
		homepage.PopUp();
		headerPageObject = new HeaderPageObject(driver);
	}

	@Test(priority = 1,description="Verify Header Main Categories")
	public void verifyHeadercategories() throws Exception {
		List<WebElement> categories = headerPageObject.Retrievecategory();
		String[] expectedcategories = FetchDataUtility.getRowSplitData(file, sheetName,
				FetchDataUtility.getCellData(file, sheetName, 1, 2));
		for (int i = 0; i < expectedcategories.length; i++) {
			String expected = expectedcategories[i];
			assertEquals(categories.get(i).getText().toString(), expected);
		}
	}

	@Test(priority = 2, dataProvider = "level1",description="Verify Header Categories Level 1: category-1,2,3,4")
	public void verifyCategoriesLevel1(String Category) throws Exception {
		List<WebElement> level1 = headerPageObject.retrieveLevel1Categories(Category);
		String[] expectedLevel1 = FetchDataUtility.getRowSplitData(file, sheetName, Category);
		if (expectedLevel1 != null)
			for (int i = 0; i < expectedLevel1.length; i++) {
				String expected = expectedLevel1[i];
				assertEquals(level1.get(i).getText().toString(), expected);
			}

	}

	@Test(priority = 3, dataProvider = "level1",description="Verify Header Categories Level 2: category* subcategory*")
	public void verifyCategoriesLevel2(String Category) throws Exception {
		List<WebElement> level1 = headerPageObject.retrieveLevel1Categories(Category);
		if (level1 != null) {
			for (WebElement element : level1) {
				String[] expectedLevel2 = FetchDataUtility.getRowSplitData(file, sheetName,
						Category.concat(element.getText()));
				List<WebElement> level2 = headerPageObject.RetrieveLevel2Categories(element);
				if (expectedLevel2 != null)
					for (int i = 0; i < expectedLevel2.length; i++) {
						assertEquals(level2.get(i).getText().toString(), expectedLevel2[i].toString());
					}
			}
		}
	}

	@Test(priority = 4,description="Verify landing pages of Header Main Categories")
	public void verifyHeaderLandingPage() throws Exception {
		for (int i = 0; i < headerPageObject.Retrievecategory().size() - 1; i++) {
			String url = driver.findElement(By.linkText(headerPageObject.Retrievecategory().get(i).getText()))
					.getAttribute("href");
			headerPageObject.Retrievecategory().get(i).click();
			waitForPageLoad(driver);
			assertTrue(driver.getCurrentUrl().contains(url));
			driver.navigate().back();
		}
	}
}
