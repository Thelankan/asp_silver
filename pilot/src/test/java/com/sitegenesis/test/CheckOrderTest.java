package com.sitegenesis.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.prevail.automation.Utility.FetchDataUtility;
import com.prevail.automation.browserHelper.BrowserBuilder;
import com.prevail.automation.pageObjects.FooterPageObjects;
import com.prevail.automation.pageObjects.HomePage;
import com.prevail.automation.pageObjects.LoginPage;

public class CheckOrderTest extends BrowserBuilder {

	public LoginPage loginpage;
	public String file = System.getProperty("user.dir") + "\\src\\test\\resources\\Testdata\\TestData.xlsx";
	public String sheet = "CheckOrder";
	private FooterPageObjects footerpageobject;
		
	@DataProvider(name = "Valid Parameters")
	public Iterator<String[]> validData() throws Exception {
		List<String[]> testObjArray = FetchDataUtility.getMultipleRowsData(file, sheet, 12, 12);
		Iterator<String[]> l = testObjArray.iterator();
		return (l);
	}


	@DataProvider(name = "Invalid Parameters")
	public Iterator<String[]> InvalidData() throws Exception {
		List<String[]> testObjArray = FetchDataUtility.getMultipleRowsData(file, sheet, 1, 11);
		Iterator<String[]> l = testObjArray.iterator();
		return (l);
	}
	

	@BeforeClass
	public void setup() {
		homepage = new HomePage(driver);
		homepage.PopUp();
	}

	@BeforeMethod
	public void beforeTestMethod() {
		footerpageobject = new FooterPageObjects(driver);
		footerpageobject.clickCheckOrder();
		loginpage = new LoginPage(driver);
	}
	

	@Test(priority = 1, dataProvider = "Valid Parameters")
	public void verifySuccessfulCheckOrder(String tcNo,String TestDescription, String OrderNumber, String OrderEmail,
			String BillingZIPcode, String ErrorValidation) throws Exception {
		loginpage.CheckOrder(OrderNumber, OrderEmail, BillingZIPcode);
		assertTrue(driver.getCurrentUrl().contains(OrderNumber));
	}

	@Test(priority = 2, dataProvider = "Invalid Parameters")
	public void verifyInvalidCheckOrder(String tcNo, String TestDescription, String OrderNumber, String OrderEmail,
			String BillingZIPcode, String ErrorValidation) throws Exception {
		loginpage.CheckOrder(OrderNumber, OrderEmail, BillingZIPcode);
		assertEquals(loginpage.getErrorMsg(TestDescription), ErrorValidation);
	}
	 

}
